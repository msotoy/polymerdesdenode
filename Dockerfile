# Imagen base
FROM node:latest

# Directorio de la app
WORKDIR /app

# Copio archivos
ADD /build/default /app/buid/default
ADD server.js /app
ADD package.json /app

# Dependencias
RUN npm install

# Puesto que expongo
EXPOSE 3000

# Comando
CMD ["npm", "start"]
